# stm32f1

## Software and Hardware

### Stm32f103c8t6

download [stm32f103c8t6](https://github.com/ve3wwg/stm32f103c8t6.git)

```bash
$ git clone https://github.com/ve3wwg/stm32f103c8t6.git
```
get in to stm32f103c8t6.

```bash
$ cd stm32f103c8t6
```

### Libopencm3

add libopencm3 as a submodule to stm32f103c8t6.

```bash
$ git submodule add https://github.com/libopencm3/libopencm3.git libopencm3
```

### FreeRTOS

Download [FreeRTOSv10.x.y](https://sourceforge.net/projects/freertos/) and extract.

```bash
$ unzip FreeRTOSv10.x.y.zip
```
copy and paste FreeRTOSv10.x.y inside stm32f103c8t6/rtos directory.

```bash
$ cp -R FreeRTOSv10.x.y /path/stm32f103c8t6/rtos/
```
get in to stm32f103c6t8/rtos, open file Project.mk , paste and close file.

```bash
FREERTOS        ?= FreeRTOSv10.x.y
```

### ST-link V2

Install st-link v2
```bash
$ apt-get install stlink-tools
```
run st-info commands as follow

```bash
$ st-info --probe
```
```bash
Found 1 stlink programmers
serial: 493f6f06483f53564554133f
openocd: "\x49\x3f\x6f\x06\x48\x3f\x53\x56\x45\x54\x13\x3f"
  flash: 131072 (pagesize: 1024)
   sram: 20480
chipid: 0x0410
  descr: F1 Medium-density device
```

### GCC-arm

Download [gcc-arm](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads/6-2017-q2-update)

```bash
$ tar xjf gcc-arm-none-eabi-6-2017-q2-update-linux.tar.bz2
$ mv gcc-arm-none-eabi-6-2017-q2-update gcc-arm
```
change to root.

```bash
$ cp -R gcc-arm /opt/
```
add export variable to bash.

```bash
export PATH="/opt/gcc-arm/bin:$PATH"
```
you should able to test cross compiler.

```bash
$ arm-none-eabi-gcc --version
```

```bash
arm-none-eabi-gcc (GNU Tools for ARM Embedded Processors  
6-2017-q2-update) 6.3.1 20170620 (release) [ARM/embedded-  
6-­branch revision 249437]  
Copyright (C) 2016 Free Software Foundation, Inc.  
This is free software; see the source for copying  
conditions. There is NO  
warranty; not even for MERCHANTABILITY or FITNESS FOR A  
PARTICULAR PURPOSE.
```

### Execute uart

clone stm32f1 repository.

```bash
$ git clone git@gitlab.com:renzocode/stm32f1.git
```
get in to stm32f1/rtos/uart and compile main.c

```bash
$ make 
```

```bash
arm-none-eabi-size uart.elf
   text	   data	    bss	    dec	    hex	filename
   3816	     16	  17648	  21480	   53e8	uart.elf
```

delete files created 

```bash
$ make clean 
$ make clobber
```

Deleted

```bash
*.o *.d *.elf *.bin
*.hex *.srec *.list *.map
```




